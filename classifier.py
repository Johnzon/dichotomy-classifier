'''
Created on Jun 2, 2012

@author: vinnie
'''

import sys
import numpy as np
import pandas as pd
from collections import defaultdict
from sklearn.neighbors import KNeighborsClassifier

def linear_decision(neighbors):
    '''
    Linear weighted nearest neighbor classification for m=[0,(k*(k+1))/2)
    '''

    w = np.arange(neighbors.size, 0, -1)[neighbors].sum()
    m = np.arange(int(neighbors.size*(neighbors.size+1))/2)
    decisions = w >= m
    return decisions

def classify(reference, labels, query, knn_function, k, p):
    neigh = KNeighborsClassifier(n_neighbors=k, p=p)
    neigh.fit(reference, labels)
    distances, nn_idx = neigh.kneighbors(query)

    sorted = distances.flatten().argsort()
    
    nn_labels = labels[nn_idx.flatten()][sorted]
    nn_dist = distances.flatten()[sorted]
    
    return knn_function(nn_labels[:k]), nn_labels[:k], nn_dist[:k]

def nn_to_df(nn):
    
    df = pd.DataFrame.from_items([((r,q,s),nn[r][q][s]) for r in nn.keys() for q in nn[r].keys() for s in nn[r][q].keys()]).T
    df.index = pd.MultiIndex.from_tuples(df.index)
    df.index.names = ['reference_user','query_user','query_session']
    return df

def pdiff(a):
    '''Pointwise differences'''
    return np.array([np.abs(xi-yi) for i,yi in enumerate(a) for xi in a[i+1:]])

def cdiff(a, b):
    '''Pointwise differences between two matrices'''
    return np.array([np.abs(xi-yi) for yi in a for xi in b])

def query_difference(reference, user, query_session):
    query_diff = cdiff(query_session.values, reference.loc[user].values)
    return query_diff

def reference_difference(reference, user, max_within_size, max_between_size):
    '''
    Create the reference difference space
    '''
    within_samples = reference.loc[user].values # M(M-1)/2
    if max_within_size:
        within_idx = np.arange(len(within_samples))
        np.random.shuffle(within_idx)
        within_samples = within_samples[within_idx[:max_within_size]] 
    within = pdiff(within_samples)
    
    other_users = reference.reset_index(level=1, drop=True).loc[set(reference.index.levels[0]) - set([user])].values
    if max_between_size:
        other_idx = np.arange(len(other_users))
        np.random.shuffle(other_idx)
        other_users = other_users[other_idx[:max_between_size]]
    between = cdiff(reference.loc[user].values, other_users)
    
    reference_diff = np.concatenate([within, between])
    labels = np.array([True]*len(within) + [False]*len(between))
    return reference_diff, labels

def validate(reference, query, k, p, max_within_size=None, max_between_size=None):

    # Linear decision
    knn_function = linear_decision
    nn_columns = int(k*(k+1))/2

    # Store the decisions in a dict and convert to DataFrame later (more efficient than setting elements in a DF)
    nn_decisions = defaultdict(lambda: defaultdict(dict))
    nn_labels = defaultdict(lambda: defaultdict(dict))
    nn_dist = defaultdict(lambda: defaultdict(dict))

    # Work is NxM, for N users and M sessions
    n_users = len(reference.index.levels[0].unique())
    n_sessions = len(query)
    work = n_users * n_sessions
    work_done = 0.0
    for reference_user in reference.index.levels[0].unique():
        reference_diff, labels = reference_difference(reference, reference_user, max_within_size, max_between_size)
        for (query_user,query_session_idx) in query.index:
            query_session = query.loc[[(query_user,query_session_idx)]]
            query_diff = query_difference(reference, reference_user, query_session)
            
            decisions, nnlabels, distances = classify(reference_diff, labels, query_diff, knn_function, k, p)
            
            nn_decisions[reference_user][query_user][query_session_idx] = decisions
            nn_labels[reference_user][query_user][query_session_idx] = nnlabels
            nn_dist[reference_user][query_user][query_session_idx] = distances
            
            work_done += 1
            sys.stdout.write('\r%.2f%% Authenticating: %s as %s for sample %s                       '
                             %(100*work_done/work, query_user, reference_user, query_session_idx))
            sys.stdout.flush()
    sys.stdout.write('\n')

    return nn_to_df(nn_decisions), nn_to_df(nn_labels), nn_to_df(nn_dist)

def loo_validate(features, k, p, max_within_size=None, max_between_size=None):

    # Linear decision
    knn_function = linear_decision
    nn_columns = int(k*(k+1))/2

    # Store the decisions in a dict and convert to DataFrame later (more efficient than setting elements in a DF)
    nn_decisions = defaultdict(lambda: defaultdict(dict))
    nn_labels = defaultdict(lambda: defaultdict(dict))
    nn_dist = defaultdict(lambda: defaultdict(dict))

    # Work is NxM, for N users and M sessions
    n_users = len(features.index.levels[0].unique())
    n_sessions = len(features)
    work = n_users * n_sessions
    work_done = 0.0
    for (query_user,query_session_idx) in features.index:
        query_session = features.loc[[(query_user,query_session_idx)]]
        loo = features.drop((query_user,query_session_idx))

        for reference_user in features.index.levels[0].unique():
            sys.stdout.write('\r%.2f%% Authenticating: %s as %s for sample %s                       '
                             %(100*work_done/work, query_user, reference_user, query_session_idx))
            sys.stdout.flush()
            
            reference_diff, labels = reference_difference(loo, reference_user, max_within_size, max_between_size)
            query_diff = query_difference(loo, reference_user, query_session)
            decisions, nnlabels, distances = classify(reference_diff, labels, query_diff, knn_function, k, p)
            nn_decisions[reference_user][query_user][query_session_idx] = decisions
            nn_labels[reference_user][query_user][query_session_idx] = nnlabels
            nn_dist[reference_user][query_user][query_session_idx] = distances
            
            work_done += 1
    sys.stdout.write('\n')

    return nn_to_df(nn_decisions), nn_to_df(nn_labels), nn_to_df(nn_dist)

def ROC(nn):
    '''
    Calculate the FAR and FRR for m from 0 to m_max over the population
    '''

    m_max = len(nn.columns)
    frr = np.zeros(m_max)
    far = np.zeros(m_max)

    within_user = nn.index.get_level_values(0)==nn.index.get_level_values(1)
    between_user = within_user==False

    cp = (nn[within_user]==True).sum()
    fn = (nn[within_user]==False).sum()
    cn = (nn[between_user]==False).sum()
    fp = (nn[between_user]==True).sum()

    far = fp/(fp+cn)
    frr = fn/(fn+cp)
    roc = pd.concat([far, frr, fp, cn, fn, cp], axis=1)
    roc = roc.reset_index()
    roc.columns = ['m', 'far','frr', 'fp', 'cn', 'fn', 'cp']
    roc[['far','frr']] *= 100
    return roc

#
# line segment intersection using vectors
# see Computer Graphics by F.S. Hill
#
def perp(a):
    
    b = np.empty_like(a)
    b[0] = -a[1]
    b[1] = a[0]
    return b

# line segment a given by endpoints a1, a2
# line segment b given by endpoints b1, b2
def seg_intersect(a1, a2, b1, b2):
    
    da = a2-a1
    db = b2-b1
    dp = a1-b1
    dap = perp(da)
    denom = np.dot( dap, db)
    num = np.dot( dap, dp )
    return (num / denom)*db + b1

def EER(roc):

    d = roc['far'] <= roc['frr']

    far_line = roc[d.diff().fillna(False) | d.diff().shift(-1)][['m','far']].values
    frr_line = roc[d.diff().fillna(False) | d.diff().shift(-1)][['m','frr']].values

    return seg_intersect(far_line[0], far_line[1], frr_line[0], frr_line[1])

def user_error(nn):

    m, eer = EER(ROC(nn))
    m = int(round(m))

    # Within user authentications
    within_user = nn[nn.index.get_level_values(0)==nn.index.get_level_values(1)]
    # Between user authentications
    between_user = nn[nn.index.get_level_values(0)!=nn.index.get_level_values(1)]

    roc_model = nn.groupby(level=0).apply(ROC)
    roc_user = nn.groupby(level=1).apply(ROC)

    eer_model = roc_model.groupby(level=0).apply(lambda x: pd.Series(EER(x)))
    eer_model.columns = ['m', 'eer']
    eer_user = roc_user.groupby(level=0).apply(lambda x: pd.Series(EER(x)))
    eer_user.columns = ['m', 'eer']

    frr_user = within_user.groupby(level=0).apply(ROC)
    far_model = between_user.groupby(level=0).apply(ROC)
    far_user = between_user.groupby(level=1).apply(ROC)

    user_frr_m = frr_user.groupby(level=0).apply(lambda x: x.iloc[m]['frr'])
    model_far_m = far_model.groupby(level=0).apply(lambda x: x.iloc[m]['far'])
    user_far_m = far_user.groupby(level=0).apply(lambda x: x.iloc[m]['far'])

    user_errors = pd.concat([user_frr_m, model_far_m, user_far_m], axis=1)
    user_errors.columns = ['frr_user', 'far_model', 'far_user']

    return user_errors, roc_model, eer_model, roc_user, eer_user

def user_performance(nn):
    '''
    Calculate the FAR and FRR for each user.
    FAR is broken down into user and auth
        far_model is the rate at which a user is authenticated by an impostor
        far_user is the rate at which an impostor (the user) can authenticate as other users
    FRR is calculated normally
        frr_user is the rate at which a user is denied authentication as self
    '''
    users = set()
    [users.update(v) for v in nn.keys()]
    
    m_max = len(list(list(nn.values())[0].values())[0])

    frr_user = {}
    far_model = {}
    far_user = {}
    
    for u in users:
        frr_user[u] = np.zeros(m_max)
        far_model[u] = np.zeros(m_max)
        far_user[u] = np.zeros(m_max)
        
    fp_user = {}
    fn_user = {}
    cp_user = {}
    cn_user = {}
    fp_auth = {}
    cn_auth = {}
    
    for m in range(m_max):
        for u in users:
            fp_user[u] = 0.0
            fn_user[u] = 0.0
            cp_user[u] = 0.0
            cn_user[u] = 0.0
            fp_auth[u] = 0.0
            cn_auth[u] = 0.0
            
        for user,auth in nn.keys():
            
            for e in nn[(user,auth)].values():
                if user == auth:
                    if e[m] == 'w':
                        cp_user[user] += 1
                    else:
                        fn_user[user] += 1
                else:
                    if e[m] == 'w':
                        fp_user[user] += 1
                        fp_auth[auth] += 1
                    else:
                        cn_user[user] += 1
                        cn_auth[auth] += 1
                        
        for u in users:
            if fn_user[u] > 0:
                frr_user[u][m] = fn_user[u]/(fn_user[u]+cp_user[u])
            if fp_user[u] > 0:
                far_model[u][m] = fp_user[u]/(fp_user[u]+cn_user[u])
            if fp_auth[u] > 0:
                far_user[u][m] = fp_auth[u]/(fp_auth[u]+cn_auth[u])

    return frr_user, far_model, far_user


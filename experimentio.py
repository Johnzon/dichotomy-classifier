'''
Created on Oct 5, 2012

@author: vinnie
'''

import os
import csv
import numpy as np
import matplotlib.pyplot as plt
from math import ceil
from collections import defaultdict

from classifier import user_performance, EER, ROC

EXPERIMENT_DIR = os.path.join(os.getenv('HOME'), 'experiments')
if not os.path.exists(EXPERIMENT_DIR):
        os.mkdir(EXPERIMENT_DIR)

def save_experiment(experiment, metadata, nn):

    out_dir = os.path.join(EXPERIMENT_DIR, experiment)
    if not os.path.exists(out_dir):
        os.mkdir(out_dir)

    os.chdir(out_dir)
    save_meta(metadata)
    save_nn(nn)
    return

def load_roc(filename):
    
    r = csv.reader(open(filename, 'rt'))
    next(r) # skip header
    frr, far = [], []
    for row in r:
        frr.append(row[1])
        far.append(row[2])
        
    return np.array(frr), np.array(far)
    
def save_roc(out_dir, frr, far, ratios):
    
    frr = np.array(frr)*100
    far = np.array(far)*100
    
    # roc values
    filename = os.path.join(out_dir, 'roc.csv')
    w = csv.writer(open(filename, 'wt'))
    w.writerow(['m', 'FRR', 'FAR'])
    
    for m,(x,y) in enumerate(zip(frr, far)):
        w.writerow([m, x, y])

    # within/between classifications
    filename = os.path.join(out_dir, 'roc_ratio.csv')
    w = csv.writer(open(filename, 'wt'))
    w.writerow(['m', 'FN', 'CP', 'FP', 'CN'])
    
    for m,(fn, cp, fp, cn) in enumerate(zip(*ratios)):
        w.writerow([m, fn, cp, fp, cn])
    
    # error and roc graphs
    m_max = len(far)
    
    plt.clf()
    plt.axis([0,m_max,0,100])
    plt.xlabel('m')
    plt.ylabel('Error rate (%)')
    plt.plot(np.linspace(0, m_max, m_max), frr, label='FRR')
    plt.plot(np.linspace(0, m_max, m_max), far, label='FAR')
    plt.legend()
    plt.savefig(os.path.join(out_dir, 'error.png'))
    
    plt.clf()
    plt.axis([0,100,0,100])
    plt.xlabel('FRR (%)')
    plt.ylabel('FAR (%)')
    plt.plot(frr, far)
    plt.savefig(os.path.join(out_dir, 'roc.png'))
    
    return

def save_meta(out_dir, metadata):
    
    w = csv.writer(open('summary.csv', 'wt'))
    w.writerows(metadata)
    return

def save_user_performance(user_performance, type):
    
    filename = type +'.csv'
    sorted_keys = sorted(user_performance.keys())
    m_max = len(list(user_performance.values())[0])
    w = csv.writer(open(filename, 'wt'))
    w.writerow(['m']+sorted_keys)
    for m in range(m_max):
        row = [m] + [user_performance[user][m] for user in sorted_keys]
        w.writerow(row)
        
    return

def save_nn(nn):

    w = csv.writer(open('nn.csv', 'wt'))
    m_max = len(list(list(nn.values())[0].values())[0])
    w.writerow(['USER', 'AUTH', 'SAMPLE']+list(range(m_max)))
    for (user,auth),tests in sorted(nn.items()):
        for sample,neighbors in sorted(tests.items()):  
            w.writerow([user, auth, sample] + neighbors)

    return

def load_nn(filename):

    reader = csv.reader(open(filename, 'rt'))
    next(reader) # skip header
    
    nn = defaultdict(dict)
    for row in reader:
        if len(row):
            nn[(row[0],row[1])][int(row[2])] = row[3:]
        
    return dict(nn)

def nn_stats(filename):

    nn = load_nn(filename)
    
    frr, far, ratios = ROC(nn)
    
    eer, m = EER(frr, far)
    
    users = set()
    [users.update(u) for u in nn.keys()]
    
    frr_user, far_model, far_user = user_performance(nn)
    
    save_user_performance(frr_user, 'frr_user')
    save_user_performance(far_model, 'far_model')
    save_user_performance(far_user, 'far_user')
    
    n_samples = ceil(np.average([len(s) for (u,a),s in nn.items() if u == a]))
    n_users = len(users)
    
    # for N users with M samples each,
    # each user has M within authentications and (N-1)*M between auths
    # Over the population, there are N*M within auths performed
    # and N*(N-1)*M between auths 
    n_within_bins = n_samples
    n_between_bins = (n_users - 1)*n_samples
    
    save_user_error_histogram(users, frr_user, 'frr_user', m, n_within_bins, cumulative=True)
    save_user_error_histogram(users, far_model, 'far_model', m, n_between_bins, cumulative=True)
    save_user_error_histogram(users, far_user, 'far_user', m, n_between_bins, cumulative=True)

    save_user_roc(users, frr_user, far_model, 'roc-model.png')
    save_user_roc(users, frr_user, far_user, 'roc-user.png')
    
    save_roc(frr, far, ratios)
    
    return

def save_user_roc(users, frr_u, far_u, output=None):

    plt.clf()
    plt.xlabel('FRR (%)')
    plt.ylabel('FAR (%)')
    for u in users:
        plt.plot(frr_u[u]*100, far_u[u]*100)
    
    if output:
        plt.savefig(output)
    else:
        plt.show()
    return

def save_user_error_histogram(users, out_dir, user_error, error_type, m, n_bins, cumulative=False):
    
    plt.clf()
    a = np.array([user_error[u][m] for u in users])*100
    hist, bins = np.histogram(a, bins=n_bins, density=True, range=(0,100))
    widths = np.diff(bins)
    hist *= 100.0/np.sum(hist)
    plt.xlabel(error_type.upper() + ' (%)')
    plt.ylabel('USERS (%)')
    plt.axis([0,100,0,100])
    plt.grid(True)
    if cumulative:
        plt.bar(bins[:-1], np.cumsum(hist), widths, color='red', alpha=0.2, linewidth=0)
    plt.bar(bins[:-1], hist, widths, linewidth=0)
    out_file = os.path.join(out_dir, error_type + '_m-'+ str(m) + '.png')
    plt.savefig(out_file)
    return

if __name__ == '__main__':
    pass

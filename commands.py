'''
Created on Apr 10, 2013

@author: vinnie
'''

import os
import pandas as pd
import experiment as exp
from argparse import ArgumentParser

load_features = lambda filename, label_col: pd.read_csv(filename, index_col=[label_col,'session'])

EXPERIMENT_DIR = os.path.join(os.getenv('HOME'), 'experiments')
if not os.path.exists(EXPERIMENT_DIR):
        os.mkdir(EXPERIMENT_DIR)

def _classifier_arguments(parser):
    parser.add_argument('--k', default=21, type=int, help='K nearest neighbors.')
    parser.add_argument('--p', default=2, type=int, help='Minkowsi parameter: p=2 for Euclidean distance.')
    parser.add_argument('--max_between_size', type=int, default=None, help='Maximum size for between class difference space')
    parser.add_argument('--max_within_size', type=int, default=None, help='Maximum size for within class difference space')
    parser.add_argument('--out_dir', default=EXPERIMENT_DIR, help='Output directory')
    parser.add_argument('--comment', default='', help='Description of the experiment')
    parser.add_argument('--label_col', default='user', help='Column containing the class labels')
    parser.add_argument('--normalize', default=None, help='Normalization method')
    parser.add_argument('--norm_distance', default=2, help='Normalization distance')
    return

def auth(args):
    '''
    Run and obtain authentication results on a reg set against a ref set.
    '''

    parser = ArgumentParser(prog='%s %s' % ("bas", "auth"))
    parser.add_argument('reference', help='Reference set fetaure vectors.')
    parser.add_argument('query', help='Query set feature vectors.')
    _classifier_arguments(parser)
    args = parser.parse_args(args)
    
    r = load_features(args.reference, args.label_col)
    q =load_features(args.query, args.label_col)
    
    if args.normalize:
        r = exp.normalize(r, args.normalize, args.norm_distance)
        q = exp.normalize(q, args.normalize, args.norm_distance)
    
    exp.auth(load_features(args.reference, args.label_col), load_features(args.query, args.label_col), args)
    return

def auth_loo(args):
    '''
    Run and obtain authentication results using LOO verification
    (Ref set is recomputed for every sample left out)
    '''
     
    parser = ArgumentParser(prog='%s %s' % ("bas", "auth_loo"))
    parser.add_argument('features', help='Feature vectors for reg set.')
    _classifier_arguments(parser)
    args = parser.parse_args(args)
    df = load_features(args.features, args.label_col)
    
    if args.normalize:
        df = exp.normalize(df, args.normalize, args.norm_distance)
    
    exp.auth_loo(df, args)
    return

def auth_rrs(args):
    '''
    Run and obtain authentication results
    '''
     
    parser = ArgumentParser(prog='%s %s' % ("bas", "auth_rrs"))
    parser.add_argument('features', help='Feature vectors for reg set.')
    parser.add_argument('--n_repeated', type=int, default=3, help='')
    parser.add_argument('--query_size', type=int, default=5, help='')
#     parser.add_argument('--query_size_percent', type=float, default=10, help='')
    parser.add_argument('--reference_reduction', type=str, default=None, help='')
    parser.add_argument('--reference_size', type=int, default=20, help='')
    _classifier_arguments(parser)
    args = parser.parse_args(args)
    
    df = load_features(args.features, args.label_col)
    
    if args.normalize:
        df = exp.normalize(df, args.normalize, args.norm_distance)
    
    exp.auth_rrs(df, args)
    return
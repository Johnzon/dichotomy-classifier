'''
Created on Oct 5, 2012

@author: vinnie
'''

import re
import os
from time import time
from random import shuffle
import matplotlib.pyplot as plt

from classifier import *

expname = lambda s: re.sub('[^0-9a-zA-Z_-]+', '.', s)

def error_sessions(nn, m):
    
    nn = nn[m]
    
    positive_auths = nn[nn.index.get_level_values(0)==nn.index.get_level_values(1)]
    negative_auths = nn[nn.index.get_level_values(0)!=nn.index.get_level_values(1)]
    
    frr_sessions = positive_auths[positive_auths==False]
    far_sessions = negative_auths[negative_auths]
    
    frr_sessions.name = 'authenticated'
    far_sessions.name = 'authenticated'
    
    return frr_sessions.reset_index().set_index('reference_user'), far_sessions.reset_index().set_index('reference_user') 

def _user_error_histogram(out_dir, a, error_type, n_bins, cumulative=False):
    
    plt.clf()
    hist, bins = np.histogram(a, bins=n_bins, density=True, range=(0,100))
    widths = np.diff(bins)
    hist *= 100.0/np.sum(hist)
    plt.xlabel(error_type.upper() + ' (%)')
    plt.ylabel('USERS (%)')
    plt.axis([0,100,0,100])
    plt.grid(True)
    if cumulative:
        plt.bar(bins[:-1], np.cumsum(hist), widths, color='red', alpha=0.2, linewidth=0)
    plt.bar(bins[:-1], hist, widths, linewidth=0)
    plt.savefig(os.path.join(out_dir, error_type + '.png'))
    return

def _user_error_output(nn_decisions_fname, out_dir=None):
    
    if not out_dir:
        out_dir = os.path.split(nn_decisions_fname)[0]
    
    nn_decisions = pd.read_csv(nn_decisions_fname, index_col=['reference_user','query_user','query_session'])
    nn_decisions.columns = range(len(nn_decisions.columns))
    m, eer = EER(ROC(nn_decisions))
    m = int(round(m))
    
    frr_sessions, far_sessions = error_sessions(nn_decisions, m)
    
    frr_sessions.to_csv(os.path.join(out_dir, 'frr_sessions_eer.csv'))
    far_sessions.to_csv(os.path.join(out_dir, 'far_sessions_eer.csv'))
    
    user_errors, roc_model, eer_model, roc_user, eer_user = user_error(nn_decisions)
    user_errors.to_csv(os.path.join(out_dir, 'user_errors.csv'))
    roc_model.to_csv(os.path.join(out_dir, 'roc_model.csv'))
    eer_model.to_csv(os.path.join(out_dir, 'eer_model.csv'))
    roc_user.to_csv(os.path.join(out_dir, 'roc_user.csv'))
    eer_user.to_csv(os.path.join(out_dir, 'eer_user.csv'))
 
    frr_bins = np.round(nn_decisions.groupby(level=[1,2]).size().mean()) # avg number of sessions per user
    far_bins = np.sqrt(len(nn_decisions))
    
    _user_error_histogram(out_dir, user_errors['frr_user'], 'frr_user', frr_bins, True)
    _user_error_histogram(out_dir, user_errors['far_model'], 'far_model', far_bins, True)
    _user_error_histogram(out_dir, user_errors['far_user'], 'far_user', far_bins, True)
    
    return

def _output(out_dir, metadata, nn_decisions, nn_labels, nn_dist):
    
    exp_dir = os.path.join(out_dir, metadata['experiment'])
    
    if not os.path.exists(exp_dir):
        os.mkdir(exp_dir)
    os.chdir(exp_dir)
    
    nn_decisions.to_csv('nn_decisions.csv')
    nn_labels.to_csv('nn_labels.csv')
    nn_dist.to_csv('nn_distances.csv')

    roc = ROC(nn_decisions)
    roc.to_csv('roc.csv')

    plt.clf()
    plt.plot(roc['frr'],roc['far'])
    plt.axis([0,100,0,100])
    plt.xlabel('FRR (%)')
    plt.ylabel('FAR (%)')
    plt.savefig('roc.png')

    plt.clf()
    plt.plot(roc['frr'], label='FRR')
    plt.plot(roc['far'], label='FAR')
    plt.axis([0,len(roc.index),0,100])
    plt.xlabel('m')
    plt.ylabel('Error rate (%)')
    plt.legend()
    plt.savefig('error.png')

    metadata['eer (m)'], metadata['eer (%)'] = EER(roc)

    df = pd.DataFrame.from_dict(metadata, orient='index')
    df.index.name = 'Attribute'
    df.columns = ['Value']
    df = df.sort_index()
    df.to_csv('metadata.csv')
    
    return

def split_sessions(features, query_size):
    '''
    Split the features into reference and query sets 
    '''
    shuffled_sessions = features.groupby(level=0).apply(lambda x: list(x.index.get_level_values(1)))
    shuffled_sessions.apply(shuffle)

    reference_sessions = shuffled_sessions.apply(lambda x: x[query_size:]).to_dict()
    query_sessions = shuffled_sessions.apply(lambda x: x[:query_size]).to_dict()

    reference = features.loc[[(k,v) for k,sessions in reference_sessions.items() for v in sessions]]
    query = features.loc[[(k,v) for k,sessions in query_sessions.items() for v in sessions]]

    return reference, query

def split_sessions_percent(features, query_size_percent):
    '''
    Split the features into reference and query sets
    '''
    shuffled_sessions = features.groupby(level=0).apply(lambda x: list(x.index.get_level_values(1)))
    shuffled_sessions.apply(shuffle)

    reference_sessions = shuffled_sessions.apply(lambda x: x[int(round(len(x)*query_size_percent)):]).to_dict()
    query_sessions = shuffled_sessions.apply(lambda x: x[:int(round(len(x)*query_size_percent))]).to_dict()

    reference = features.loc[[(k,v) for k,sessions in reference_sessions.items() for v in sessions]]
    query = features.loc[[(k,v) for k,sessions in query_sessions.items() for v in sessions]]

    return reference, query


def auth(reference, query, args):

    metadata = args.__dict__.copy()
    metadata['validation'] = 'Full query vs. reference set'
    metadata['experiment'] = expname('%s__%s' %(args.reference, args.query))
    
    metadata['num_users'] = len(query.index.levels[0].unique())
    metadata['num_sessions'] = len(query.index)
    metadata['num_users_reference'] = len(reference.index.levels[0].unique())
    metadata['num_sessions_reference'] = len(reference.index)
    metadata['num_features'] = len(query.columns)

    print("Users:", metadata['num_users'], "Samples:", metadata['num_sessions'], "Features: ", metadata['num_features'])

    start_time = time()
    nn_decisions, nn_labels, nn_distances = validate(reference, query, args.k, args.p, args.max_within_size, args.max_between_size)
    metadata['duration'] = time() - start_time

    _output(args.out_dir, metadata, nn_decisions, nn_labels, nn_distances)
    return

def auth_loo(features, args):
    
    metadata = args.__dict__.copy()
    metadata['validation'] = 'Leave-one-out cross validation'
    metadata['experiment'] = expname(args.features)

    metadata['num_users'] = len(features.index.levels[0].unique())
    metadata['num_sessions'] = len(features.index)
    metadata['num_features'] = len(features.columns)

    print("Users:", metadata['num_users'], "Samples:", metadata['num_sessions'], "Features: ", metadata['num_features'])

    start_time = time()
    nn_decisions, nn_labels, nn_distances = loo_validate(features, args.k, args.p, args.max_within_size, args.max_between_size)
    metadata['duration'] = time() - start_time

    _output(args.out_dir, metadata, nn_decisions, nn_labels, nn_distances)
    return

def auth_rrs(features, args):

    metadata = args.__dict__.copy()
    metadata['validation'] = 'Repeated random subsampling (RRS)'
    metadata['experiment'] = expname(args.features)

    metadata['num_users'] = len(features.index.levels[0].unique())
    metadata['num_sessions'] = len(features.index)
    metadata['num_features'] = len(features.columns)

    print("Users:", metadata['num_users'], "Samples:", metadata['num_sessions'], "Features: ", metadata['num_features'])

    nn_decisions_repeated = []
    nn_labels_repeated = []
    nn_distances_repeated = []
    start_time = time()
    for i in range(args.n_repeated):
#         reference, query = split_sessions_percent(features, args.query_size_percent/100)
        reference, query = split_sessions(features, args.query_size)
        nn_decisions, nn_labels, nn_distances = validate(reference, query, args.k, args.p, max_within_size=args.max_within_size, max_between_size=args.max_between_size)
        nn_decisions_repeated.append(nn_decisions)
        nn_labels_repeated.append(nn_labels)
        nn_distances_repeated.append(nn_distances)
        
        
    metadata['duration'] = time() - start_time

    nn_decisions = pd.concat(nn_decisions_repeated, join='outer')
    nn_labels = pd.concat(nn_labels_repeated, join='outer')
    nn_distances = pd.concat(nn_distances_repeated, join='outer')
    
    _output(args.out_dir, metadata, nn_decisions, nn_labels, nn_distances)

    return

def normalize(df, method, distance=2):
    
    if method == 'stddev':
        for col in df.columns - ['user', 'session']:
            upper = df[col].mean() + distance*df[col].std()
            lower = df[col].mean() - distance*df[col].std()
            df[col] = (df[col] - lower)/(upper - lower)
            df[col][df[col] < 0] = 0
            df[col][df[col] > 1] = 1
    elif method == 'minmax':
        for col in df.columns - ['user', 'session']:
            df[col] = (df[col] - df[col].min())/(df[col].max() - df[col].min())
    else:
        print('Invalid normalization method')
        return None
        
    return df
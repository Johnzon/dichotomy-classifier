'''
Created on May 16, 2013

@author: vinnie
'''

import os
from itertools import product

def setproduct(*a):
    return set(product(*a))

def listoftuples(l):
    return [(e,) for e in l]

def tupleoftuples(l):
    return tuple((e,) for e in l)

def dirhead_fileroot(filename):
    return os.path.split(filename)[0], os.path.splitext(os.path.split(filename)[1])[0]

def load_lines(filename):
    f = open(filename, 'rt')
    return [l[:-1] for l in f] # drop the \n

def ngrams(tokens, MIN_N, MAX_N):
    n_tokens = len(tokens)
    for i in range(n_tokens):
        for j in range(i+MIN_N, min(n_tokens, i+MAX_N)+1):
            yield tokens[i:j]